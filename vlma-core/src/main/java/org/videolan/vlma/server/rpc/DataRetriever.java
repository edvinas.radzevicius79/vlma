/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.videolan.vlma.model.Server;

public interface DataRetriever {

    public static enum Method {
        SNMP,
        HTTP,
        NONE
    }

    Map<ServerState, Double> getServerState(Server server);

    String getVlcVersion(Server server) throws IOException;

    List<String> getVlcLogTail(Server server) throws IOException;

    Long getVlcUptime(Server server) throws IOException;

    boolean restartVlc(Server server) throws IOException;

}
