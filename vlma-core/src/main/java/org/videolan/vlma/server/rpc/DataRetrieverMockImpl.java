/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.videolan.vlma.model.Server;

public class DataRetrieverMockImpl implements DataRetriever {

    public Map<ServerState, Double> getServerState(Server server) {
        Map<ServerState, Double> result = new HashMap<ServerState, Double>();
        for(ServerState data : ServerState.values()) {
            result.put(data, 0D);
        }
        return result;
    }

    public List<String> getVlcLogTail(Server server) {
        return null;
    }

    public String getVlcVersion(Server server) {
        return null;
    }

    public boolean restartVlc(Server server) {
        return false;
    }

    public Long getVlcUptime(Server server) throws IOException {
        return null;
    }

}
