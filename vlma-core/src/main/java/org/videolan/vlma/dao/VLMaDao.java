/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.dao;

import java.util.Collection;
import java.util.List;

import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;

public interface VLMaDao {

    List<Media> getMedias();
    List<Satellite> getSatellites();
    List<Server> getServers();

    Media getMedia(int id);
    Satellite getSatellite(int id);
    Server getServer(int id);

    void update(Media media);
    void update(Satellite satellite);
    void update(Server server);

    void addAll(Collection<Media> medias);
    void add(Media media);
    void add(Satellite satellite);
    void add(Server server);

    void removeAll(Collection<Media> medias);
    void remove(Media media);
    void remove(Satellite satellite);
    void remove(Server server);

    public void loadFromDisk();
    public void saveToDisk();
}
