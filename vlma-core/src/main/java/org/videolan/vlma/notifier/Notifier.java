/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.notifier;

import java.util.ArrayList;
import java.util.List;

/**
 * A notifier.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public abstract class Notifier {

    private static List<Notifier> notifiers = new ArrayList<Notifier>();

    /**
     * Dispatch the message to registered notifiers.
     *
     * @param message the message to dispatch
     */
    public static void dispatchNotification(String message) {
        synchronized (notifiers) {
            for (Notifier notifier : notifiers) {
                notifier.sendNotification(message);
            }
        }
    }

    protected static void register(Notifier notifier) {
        synchronized (notifiers) {
            notifiers.add(notifier);
        }
    }

    protected static void unregister(Notifier notifier) {
        synchronized (notifiers) {
            notifiers.remove(notifier);
        }
    }

    /**
     * Initializes the notifier.
     */
    public void init() {
        register(this);
    }

    /**
     * Sends a notification.
     *
     * @param message the message to send
     */
    public abstract void sendNotification(String message);

    /**
     * Destroys the notifier.
     */
    public void destroy() {
        unregister(this);
    }

}
