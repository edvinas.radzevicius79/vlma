/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.watcher;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Program;

/**
 * This Watcher directly connects to the multicast groups to check whether some
 * data is broadcasted. If yes, it considers that the media is broadcasted.
 * This class should never be used directly. Use {@link StreamWatcherDispatcher}
 * instead.
 *
 * @author Adrien Maglo <magsoft at videolan.org>
 */
public class DirectMulticastStreamWatcher implements StreamWatcher {

    private static final Logger logger = Logger.getLogger(DirectMulticastStreamWatcher.class);

    /**
     * The thread used to wait.
     */
    private transient Thread waitThread;

    /**
     * The multicast socket timeout
     */
    private static final int SOCKET_RECEIVE_TIMEOUT = 1000;

    /**
     * The minimal length of the string to consider that the media is
     * broadcasted
     */
    private static final int BUF_LENGTH_MIN = 100;

    /**
     * The time leaved to the multicast socket to get data.
     */
    private static final int WAIT_SPAN = 1000;

    /**
     * Is true when the socket try to receive data
     */
    private AtomicBoolean isReceivingData;

    public DirectMulticastStreamWatcher() {
        isReceivingData = new AtomicBoolean(false);
    }

    public synchronized boolean isPlayed(Program program) {
        boolean isPlayed = false;
        logger.debug("Joining the multicast group " + program.getIp());
        MulticastSocket s;
        try {
            s = new MulticastSocket(1234);
            s.setSoTimeout(SOCKET_RECEIVE_TIMEOUT);
            s.joinGroup(program.getIp());
            byte[] buf = new byte[1024];
            DatagramPacket recv = new DatagramPacket(buf, buf.length);
            /* Try to receive some data */
            try {
                isReceivingData.set(true);
                startWaitingForTheSocket();
                long bufLength = 0;
                while (isReceivingData.get()) {
                    s.receive(recv);
                    bufLength += recv.getLength();
                    /*
                     * If the minimum data quantity has been received,
                     * then ...
                     */
                    if (bufLength >= BUF_LENGTH_MIN) {
                        /* It is no more necessary to wait */
                        isReceivingData.set(false);
                        waitThread.interrupt();
                    }
                }

                /*
                 * This is a dirty test but I haven't seen something
                 * else at this moment
                 */
                if (bufLength >= BUF_LENGTH_MIN) {
                    /*
                     * If some data has been retrieved, then
                     * the stream must be broadcasted
                     */
                    logger.log(Level.DEBUG, "Some data has been received : " + bufLength + " bytes.");
                    isPlayed = true;
                } else {
                    logger.log(Level.DEBUG, "Not enough data received (" + bufLength +
                            " bytes) verifying the program " + program.getName());
                }
            } catch (InterruptedIOException e) {
                logger.log(Level.DEBUG, "Socket TimeOut verifying the channel " + program.getName() + ".");
            }
            /* Leave the group */
            s.leaveGroup(program.getIp());
        } catch (IOException e) {
            logger.error(e);
        }
        return isPlayed;
    }

    /**
     * This wait WAIT_SPAN miliseconds for the multicast socket to receive some
     * data
     */
    private transient Runnable waiter = new Runnable() {
        public void run() {
            /* We start waiting */
            try {
                Thread.sleep(WAIT_SPAN);
                /* Now the socket must stop receiving data */
                isReceivingData.set(false);
            } catch (InterruptedException e) { }
        }
    };

    /**
     * Start the waiting thread
     */
    private synchronized void startWaitingForTheSocket() {
        waitThread = new Thread(waiter);
        waitThread.setName("waitThread");
        waitThread.start();
    }

}
