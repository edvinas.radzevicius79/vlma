/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.sender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * This class encapsulates an open socket to a telnet interface and the
 * corresponding input and output streams.
 */
public class TelnetConnection {

    /**
     * Time to wait for during telnet connections.
     */
    private static final int TELNET_WAIT_INTERVAL = 100;
    private static final int TELNET_WAIT_MAX = 5000;

    private static final int DEFAULT_SOCKET_TIMEOUT = 5000;

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public void connect(InetAddress address, int port) throws IOException {
        connect(address, port, DEFAULT_SOCKET_TIMEOUT);
    }

    public void connect(InetAddress address, int port, int timeout) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(address, port), timeout);
        out = new PrintWriter(socket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void close() throws IOException {
        if (out != null) {
            out.close();
            out = null;
        }
        if (in != null) {
            in.close();
            in = null;
        }
        if (socket != null) {
            socket.close();
            socket = null;
        }
    }

    public void println(String line) {
        out.println(line);
    }

    public boolean ready() throws IOException {
        return in.ready();
    }

    public int read() throws IOException {
        return in.read();
    }

    public void resetIn() throws IOException {
        while (ready()) read();
    }

    /**
     * Wait until the connection is ready.
     *
     * @throws IOException
     */
    public void waitUntilReady() throws IOException {
        int timeSleeped = 0;
        // Continue to wait if the telnet interface is not ready yet, but
        // not more than TELNET_WAIT_MAX ms.
        while (!ready()) {
            if (timeSleeped < TELNET_WAIT_MAX) {
                try {
                    Thread.sleep(TELNET_WAIT_INTERVAL);
                    timeSleeped += TELNET_WAIT_INTERVAL;
                } catch (InterruptedException e) {}
            } else {
                throw new IOException("Timeout while trying to contact VLC telnet interface");
            }
        }
    }

    /**
     * Run a command in the telnet interface and return the response.
     *
     * @param command the command to run
     * @return the response
     * @throws IOException
     */
    public String run(String command) throws IOException {
        println(command);
        waitUntilReady();
        StringBuilder response = new StringBuilder();
        while (ready()) {
            response.append((char) read());
        }
        // Remove the "> " which is due to the prompt
        int end = response.lastIndexOf(">");
        int responseLength = response.length();
        if (end  < 0 || end < responseLength - 4) {
            // No ending "> ", so keep the whole response
            end = responseLength;
        }
        return response.substring(0, end);
    }

}
