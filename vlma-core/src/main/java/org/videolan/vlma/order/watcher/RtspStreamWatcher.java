/*
 * Copyright (C) 2006-2008 the VideoLAN team
*
* This file is part of VLMa.
*
* VLMa is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* VLMa is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with VLMa. If not, see <http://www.gnu.org/licenses/>.
*
*/

package org.videolan.vlma.order.watcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.util.Utils;


/**
 * This class monitors VoD streams. It should never be used directly. Use
 * {@link StreamWatcherDispatcher} instead.
 *
 * @author Adrien Maglo <magsoft at videolan.org>
 */
public class RtspStreamWatcher implements StreamWatcher {

    private Configuration configuration;

    private Utils utils;

    private static final Logger logger = Logger.getLogger(DirectMulticastStreamWatcher.class);

    /**
     * The time leaved to the socket to get RTSP data.
     */
    private static final int WAIT_SPAN = 500;

    /**
     * Check if the media is played or not by asking the RTSP server
     */
    public boolean isPlayed(Program program) {
        String url = utils.getUrl(program);

        PrintWriter out = null;
        BufferedReader in = null;

        Socket mySocket;
        try {
            int port = configuration.getInt("vlma.streaming.rtsp.port");
            mySocket = new Socket(program.getPlayer().getAddress(), port);
            out = new PrintWriter(mySocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    mySocket.getInputStream()));
        } catch (UnknownHostException e) {
            logger.debug("Couldn't contact the RTSP server.", e);
            return false;
        } catch (IOException e) {
            logger.debug("Couldn't contact the RTSP server.", e);
            return false;
        }

        out.println("DESCRIBE " + url + " RTSP/1.0");
        out.println(""); /* Empty line (RTSP protocol) */

        boolean isPlayed = false;

        /* Check server answer */
        Thread waitThread = new Waiter(mySocket);
        waitThread.setName("waitThread");
        waitThread.start();
        try {
            String line = in.readLine();
            String[] resp = line.split("\\s+"); // RTSP/1.0 <code> <message>
            if (resp.length == 3 && resp[1].matches("[0-9]{3}")) {
                int code = Integer.parseInt(resp[1]);
                if (code == 200) {
                    isPlayed = true;
                    logger.debug("The RTSP media " + program.getName() + " is available.");
                } else {
                    logger.debug(String.format("The RTSP media %s is NOT available: %d code received",
                            program.getName(), code));
                }
            } else {
                logger.debug("The RTSP media " + program.getName() +
                        " is not played: Bad response format: " + line);
            }
        } catch (IOException e) {
            logger.debug("The RTSP media " + program.getName() +
                    " is not played: Timeout while waiting for the response");
            isPlayed = false;
        }

        /* Stop the thread if it hasn't already be done */
        waitThread.interrupt();

        return isPlayed;
    }

    /**
     * This wait WAIT_SPAN milliseconds for the socket to receive
     * RTSP data
     */
    private static class Waiter extends Thread {

        private Socket socket;

        public Waiter(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            /* We start waiting */
            try {
                Thread.sleep(WAIT_SPAN);
                /* Now the socket must stop receiving data */
                try {
                    socket.close();
                } catch (IOException e) {
                    /* Do nothing */
                }
            } catch (InterruptedException e) {
                logger.warn("Waiter thread was interrupted", e);
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    /* Do nothing */
                }
            }
        }

    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets utils.
     *
     * @param utils the utils to set
     */
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

}
