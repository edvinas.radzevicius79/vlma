/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.management;

import org.videolan.vlma.model.Order;

/**
 * Object wrapper to store together an order and its state.
 */
public class OrderState {

    public static enum State {
        STARTED, TO_START, TO_STOP, STOPPED
    }

    private Order order;
    private State state;

    public OrderState(Order order) {
        this.order = order;
        this.state = State.TO_START;
    }

    public Order getOrder() {
        return order;
    }

    public State getState() {
        return state;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setState(State state) {
        if (state == null) {
            throw new NullPointerException("Cannot set a null state");
        }
        this.state = state;
    }
}
