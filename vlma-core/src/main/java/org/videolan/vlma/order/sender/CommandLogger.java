/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.sender;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.videolan.vlma.model.Command;
import org.videolan.vlma.model.Server;

/**
 * Keeps the history of commands which have been sent to servers.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class CommandLogger {

    private Configuration configuration;
    private LinkedList<Command> commands = new LinkedList<Command>();

    public void add(Command command) {
        synchronized (commands) {
            commands.addFirst(command);
            while (commands.size() > configuration.getInt("vlma.ui.command.queue.size")) {
                commands.removeLast();
            }
        }
    }

    public void add(Server server, String command, String response) {
        this.add(new Command(server, command, response));
    }

    /**
     * Gets the last commands sent to servers.
     *
     * @return the last commands sent
     */
    public List<Command> getCommands() {
        synchronized (commands) {
            return commands;
        }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
