/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.io.FileUtils;
import org.htmlparser.util.ParserException;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.Media;

import com.google.common.collect.Lists;

public class SatcodxUtilsTest {

    private BaseConfiguration configuration;

    @Before
    public void setUp() {
        configuration = new BaseConfiguration();
        configuration.addProperty("category-filter", "TV-DIG");
        configuration.addProperty("category-filter", "R-DIG");
        configuration.addProperty("th.nbcols", 2);
        configuration.addProperty("td.nbcols", 8);
        configuration.addProperty("name.owner", "td");
        configuration.addProperty("name.column", 1);
        configuration.addProperty("name.row", 0);
        configuration.addProperty("frequency.owner", "th");
        configuration.addProperty("frequency.column", 0);
        configuration.addProperty("frequency.row", 1);
        configuration.addProperty("frequency.match", "/[0-9]+\\.[0-9]{3} GHz/");
        configuration.addProperty("frequency.substitutions",
                "s/\\.([0-9]{3}).*/$1/");
        configuration.addProperty("frequency.substitutions", "s/$/000/");
        configuration.addProperty("sid.owner", "td");
        configuration.addProperty("sid.column", 4);
        configuration.addProperty("sid.row", 1);
        configuration.addProperty("sid.match", "/[0-9]+/");
        configuration.addProperty("encryption.owner", "td");
        configuration.addProperty("encryption.column", 2);
        configuration.addProperty("encryption.row", 1);
        configuration.addProperty("coverage.owner", "th");
        configuration.addProperty("coverage.column", 1);
        configuration.addProperty("coverage.row", 1);
        configuration.addProperty("coverage.match", "/^[A-Z0-9]{8}$/");
        configuration.addProperty("category.owner", "td");
        configuration.addProperty("category.column", 0);
        configuration.addProperty("category.row", 0);
        configuration.addProperty("polarization.default", "A");
        configuration.addProperty("polarization.owner", "th");
        configuration.addProperty("polarization.column", 0);
        configuration.addProperty("polarization.row", 2);
        configuration.addProperty("polarization.match", "/[HhVvAa]/");
        configuration.addProperty("symbol-rate.owner", "th");
        configuration.addProperty("symbol-rate.column", 0);
        configuration.addProperty("symbol-rate.row", 3);
        configuration.addProperty("symbol-rate.match", "/[0-9]{5}/");
        configuration.addProperty("symbol-rate.substitutions", "s/$/000/");
        configuration.addProperty("fec.default", "9");
    }

    @Test
    public void testGetSatChannels() throws IOException, ParserException {
        File tmp = File.createTempFile("satcodx", ".html");
        FileUtils.copyURLToFile(this.getClass().getResource("/satcodx-sample.html"), tmp);

        List<Media> medias = Lists.newArrayList();
        SatcodxUtils.getSatChannels(tmp.toURI().toURL(),
                configuration, medias);
        tmp.delete();

        assertTrue(medias.size() > 0);
    }

}
