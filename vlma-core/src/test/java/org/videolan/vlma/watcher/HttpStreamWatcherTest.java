/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.watcher;

import static org.junit.Assert.assertFalse;

import java.util.Random;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.FilesAdapter;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamingStrategy;
import org.videolan.vlma.order.watcher.HttpStreamWatcher;
import org.videolan.vlma.order.watcher.StreamWatcher;
import org.videolan.vlma.util.Utils;

public class HttpStreamWatcherTest {

    private Configuration configuration;
    private StreamWatcher watcher;
    private Utils utils;

    @Before
    public void setup() {
        configuration = new BaseConfiguration();
        Random random = new Random();
        configuration.setProperty("vlma.streaming.http.port", 81 + random.nextInt(10000));
        utils = new Utils();
        utils.setConfiguration(configuration);
        watcher = new HttpStreamWatcher();
        ((HttpStreamWatcher)watcher).setUtils(utils);
    }

    @Test
    public void testIsPlayed() throws Exception {
        Media media = new FilesChannel();
        Program program = new Program();
        program.setMedia(media);
        program.setGroup("group");
        program.setName("name");
        StreamingStrategy strategy = new StreamingStrategy();
        strategy.setProtocol(StreamingStrategy.Protocol.HTTP);
        program.setStreamingStrategy(strategy);
        media.addProgram(program);
        Server server = new Server();
        Adapter adapter = new FilesAdapter();
        adapter.setName("pouet");
        adapter.setServer(server);
        server.addAdapter(adapter);
        program.setAdapter(adapter);
        assertFalse(watcher.isPlayed(program));
    }

}
