

<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1>
    <c:choose>
        <c:when test="${shortList}">
            <fmt:message key="medias.shortlistSat" />
        </c:when>
        <c:otherwise>
            <fmt:message key="medias.fulllistSat" />
        </c:otherwise>
    </c:choose>
</h1>

<c:choose>
<c:when test="${empty satChannels}">
    <p><fmt:message key="medias.sat.empty" /></p>
</c:when>
<c:otherwise>

<table width="100%">
    <tr>
        <th colspan="2"><fmt:message key="medias.list.frequency" /></th>
        <th><fmt:message key="medias.list.sid" /></th>
        <th><fmt:message key="medias.list.name" /></th>
        <th><fmt:message key="medias.list.action" /></th>
    </tr>

    <c:set var="frequency" value="0" />

    <c:forEach items="${satChannels}" var="satChannel" varStatus="status">
        <c:choose>
            <c:when test='${(status.index)%2 eq 0}'>
                <c:set var="rowColor" value="even" scope="page"/>
            </c:when>
            <c:otherwise>
                <c:set var="rowColor" value="odd" scope="page"/>
            </c:otherwise>
        </c:choose>
        <tr class="${rowColor}">
            <td><small>
                <c:if test="${frequency != satChannel.frequency}">
                    <c:out value="${satChannel.coverage}" />
                </c:if>
            </small></td>
            <td><small>
                <c:if test="${frequency != satChannel.frequency}">
                    <c:out value="${satChannel.frequency}" />
                </c:if>
            </small></td>
            <td><small>
                <c:out value="${satChannel.sid}" />
            </small></td>
            <c:set var="frequency" value="${satChannel.frequency}" />
            <td>
                <strong>
                    <a name="<c:out value="${satChannel.id}" />">
                        <font color="<c:choose><c:when test="${satChannel.isCrypted}">#808080</c:when><c:when test="${satChannel.isRadio}">#900000</c:when><c:otherwise>#0000F0</c:otherwise></c:choose>">
                            <c:out value="${satChannel.name}" />
                        </font>
                    </a>
                </strong>
            </td>
            <c:url value="mediaprogramadd.htm" var="programAddUrl">
                <c:param name="media" value="${satChannel.id}" />
            </c:url>
            <td><a href="${programAddUrl}"><img src="<c:url value="/img/play.png" />" title="<fmt:message key="medias.list.action.run" />" /></a></td>
        </tr>
        <c:forEach items="${satChannel.programs}" var="program" varStatus="status">
            <tr class="${rowColor}">
                <td colspan="3" />
                <td class="program"><vlma:program program="${program}" /></td>
                <c:url value="mediaprogramremove.htm" var="programRemoveUrl">
                    <c:param name="media" value="${satChannel.id}" />
                    <c:param name="program" value="${program.id}" />
                </c:url>
                <td><a href="${programRemoveUrl}"><img src="<c:url value="/img/stop.png" />" title="<fmt:message key="medias.list.action.stop" />" /></a></td>
            </tr>
        </c:forEach>
    </c:forEach>
</table>

</c:otherwise>
</c:choose>
