/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.rmi.RemoteException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.Server;

public class ServerAdapterAddValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return (arg0.equals(ServerAdapterAdd.class));
    }

    public void validate(Object arg0, Errors arg1)
    {
        ServerAdapterAdd serversAdapterAdd = (ServerAdapterAdd) arg0;

        if (serversAdapterAdd == null) {
            arg1.rejectValue("name", "servers.adapter.add.error.not-specified");
            return;
        } else {
            String adapterName = serversAdapterAdd.getName().trim();
            try {
                Integer.parseInt(adapterName);
            } catch (NumberFormatException e) {
                arg1.rejectValue("name",
                        "servers.adapter.add.error.invalidname");
            }
            Server s;
            try {
                s = data.getServer(serversAdapterAdd.getServer());
                for (Adapter a : s.getAdapters()) {
                    if (serversAdapterAdd.getName().equals(a.getName()))
                        arg1.rejectValue("name",
                                "servers.adapter.add.error.existingname");
                }
            } catch (RemoteException e1) {
                arg1.rejectValue("name", "error.remote_exception");
            }

            String type = serversAdapterAdd.getType();
            try {
                Class.forName(type);
            } catch (ClassNotFoundException e) {
                arg1.rejectValue("type",
                        "servers.adapter.add.error.invalidtype");
            }
        }

    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
