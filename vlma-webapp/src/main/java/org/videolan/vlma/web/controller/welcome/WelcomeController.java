/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.welcome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.StreamChannel;
import org.videolan.vlma.model.DTTChannel;

public class WelcomeController implements Controller {

    private static final Comparator<Program> PROGRAM_COMPARATOR = new Comparator<Program>() {

        // Sort medias according to their priority
        public int compare(Program p0, Program p1) {
            int result = p1.getPriority() - p0.getPriority();
            if(result == 0) {
                return p0.getName().compareTo(p1.getName());
            } else {
                return result;
            }
        }

    };

    public ModelAndView handleRequest(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {

        List<Program> satPrograms = new ArrayList<Program>();
        List<Program> dttPrograms = new ArrayList<Program>();
        List<Program> filePrograms = new ArrayList<Program>();
        List<Program> streamPrograms = new ArrayList<Program>();

        for (Media media : data.getMedias()) {
            for (Program program : media.getPrograms()) {
                if (media.getClass().equals(SatChannel.class)) {
                    satPrograms.add(program);
                } else if (media.getClass().equals(DTTChannel.class)) {
                    dttPrograms.add(program);
                } else if (media.getClass().equals(FilesChannel.class)) {
                    filePrograms.add(program);
                } else if (media.getClass().equals(StreamChannel.class)) {
                    streamPrograms.add(program);
                }
            }
        }

        Collections.sort(satPrograms, PROGRAM_COMPARATOR);
        Collections.sort(dttPrograms, PROGRAM_COMPARATOR);
        Collections.sort(filePrograms, PROGRAM_COMPARATOR);
        Collections.sort(streamPrograms, PROGRAM_COMPARATOR);

        ModelAndView mav = new ModelAndView();
        mav.addObject("satPrograms", satPrograms);
        mav.addObject("dttPrograms", dttPrograms);
        mav.addObject("filePrograms", filePrograms);
        mav.addObject("streamPrograms", streamPrograms);
        mav.addObject("servers", data.getServers());
        return mav;
    }

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

}
