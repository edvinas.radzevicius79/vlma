/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.dtt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.DTTChannel;

public class DTTChannelListController implements Controller {

    private static final Comparator<DTTChannel> CHANNEL_COMPARATOR = new Comparator<DTTChannel>() {

        public int compare(DTTChannel o1, DTTChannel o2) {
            int result = o1.getFrequency() - o2.getFrequency();
            if(result == 0)
                result = o1.getSid() - o2.getSid();
            return result;
        }

    };

    public ModelAndView handleRequest(HttpServletRequest arg0,
            HttpServletResponse arg1) throws Exception {
        String filter = arg0.getParameter("filter");

        // DTT channels
        List<DTTChannel> dttChannels = new ArrayList<DTTChannel>();
        for (Media media : data.getMedias()) {
            if (media instanceof DTTChannel) {
                DTTChannel channelDTT = (DTTChannel) media;
                dttChannels.add(channelDTT);
            }
        }

        Collections.sort(dttChannels, CHANNEL_COMPARATOR);

        ModelAndView mav = new ModelAndView();
        mav.addObject("dttChannels", dttChannels);
        mav.addObject("shortList", "all".equals(filter));
        return mav;
    }

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

}
