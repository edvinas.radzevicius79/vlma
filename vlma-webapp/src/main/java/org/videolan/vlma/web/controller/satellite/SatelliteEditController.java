/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.satellite;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Satellite;

public class SatelliteEditController extends SimpleFormController {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        String satelliteCoverage = ((SatelliteAdd) command).getCoverage();
        int satelliteId = ((SatelliteAdd) command).getId();
        Satellite satellite = data.getSatellite(satelliteId);
        satellite.getCoverages().clear();
        for(String coverage : satelliteCoverage.trim().split("\n")) {
            satellite.getCoverages().add(coverage.trim());
        }
        data.update(satellite);
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        SatelliteAdd satellitesAdd = new SatelliteAdd();
        int id = Integer.parseInt(request.getParameter("satellite"));
        Satellite satellite = data.getSatellite(id);
        if (satellite != null) {
            satellitesAdd.setName(satellite.getName());
            StringBuilder coverages = new StringBuilder();
            for (String coverage : satellite.getCoverages()) {
                coverages.append(coverage);
                coverages.append('\n');
            }
            satellitesAdd.setCoverage(coverages.toString());
        }
        satellitesAdd.setId(id);
        return satellitesAdd;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0,
            HttpServletResponse arg1, BindException arg2, Map arg3)
            throws Exception {
        Satellite satellite = data.getSatellite(Integer.parseInt(arg0
                .getParameter("satellite")));
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("satellite", satellite);
        return super.showForm(arg0, arg1, arg2, m);
    }

}
