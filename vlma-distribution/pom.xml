<?xml version="1.0" encoding="UTF-8"?>

<!--
  Copyright (C) 2006-2008 the VideoLAN team

  This file is part of VLMa.

  VLMa is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.

  VLMa is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with VLMa. If not, see <http://www.gnu.org/licenses/>.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <parent>
        <groupId>org.videolan.vlma</groupId>
        <artifactId>vlma-parent</artifactId>
        <version>0.3-SNAPSHOT</version>
    </parent>

    <artifactId>vlma-distribution</artifactId>
    <name>VLMa - Distribution</name>
    <description>The VLMa distribution</description>
    <modelVersion>4.0.0</modelVersion>
    <packaging>pom</packaging>

    <properties>
        <jetty.version>6.1.11</jetty.version>
        <generated.dirs>target/generated-dirs</generated.dirs>
        <apache.license>src/licenses/LICENSE-APACHE-2.0</apache.license>
        <cpl.license>src/licenses/LICENSE-CPL-1.0</cpl.license>
        <lgpl.license>src/licenses/LICENSE-LGPL</lgpl.license>
        <msnmlib.license>src/licenses/LICENSE-MSNMLIB</msnmlib.license>
        <snmp.license>src/licenses/LICENSE-SNMP</snmp.license>
        <xpp3.license>src/licenses/LICENSE-XPP3</xpp3.license>
        <xstream.license>src/licenses/LICENSE-XSTREAM</xstream.license>
    </properties>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generated_dirs</id>
                        <phase>generate-resources</phase>
                        <configuration>
                            <tasks>
                                <mkdir dir="${generated.dirs}/data" />
                                <mkdir dir="${generated.dirs}/lib/daemon" />
                                <mkdir dir="${generated.dirs}/lib/web" />
                                <mkdir dir="${generated.dirs}/logs" />
                                <mkdir dir="${generated.dirs}/work" />
                                <mkdir dir="${generated.dirs}/webapps" />
                                <mkdir dir="target/licenses" />
                            </tasks>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>copy_licenses</id>
                        <phase>process-resources</phase>
                        <configuration>
                            <tasks>
                                <copy file="${apache.license}" tofile="${generated.dirs}/lib/daemon/spring-LICENSE.txt" />
                                <copy file="${apache.license}" tofile="${generated.dirs}/lib/daemon/log4j-LICENSE.txt" />
                                <copy file="${apache.license}" tofile="${generated.dirs}/lib/daemon/commons-LICENSE.txt" />
                                <copy file="${apache.license}" tofile="${generated.dirs}/lib/web/jetty-LICENSE.txt" />
                                <copy file="${xstream.license}" tofile="${generated.dirs}/lib/daemon/xstream-LICENSE.txt" />
                                <copy file="${cpl.license}" tofile="${generated.dirs}/lib/daemon/htmlparser-LICENSE.txt" />
                                <copy file="${msnmlib.license}" tofile="${generated.dirs}/lib/daemon/msnmlib-LICENSE.txt" />
                                <copy file="${xpp3.license}" tofile="${generated.dirs}/lib/daemon/xpp3-LICENSE.txt" />
                                <copy file="${lgpl.license}" tofile="${generated.dirs}/lib/daemon/jrobin-LICENSE.txt" />
                                <copy file="${snmp.license}" tofile="${generated.dirs}/lib/daemon/snmp-LICENSE.txt" />
                                <copy file="${apache.license}" tofile="${generated.dirs}/lib/web/ant-LICENSE.txt" />
                            </tasks>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptors>
                        <descriptor>src/assembly/bin.xml</descriptor>
                    </descriptors>
                    <finalName>vlma-${project.version}</finalName>
                    <skipAssembly>${maven.assembly.skip}</skipAssembly>
                </configuration>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy_daemon_dependencies</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeScope>runtime</includeScope>
                            <outputDirectory>${project.build.directory}/generated-dirs/lib/daemon</outputDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy_web_dependencies</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>jetty</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>jetty-util</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>start</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>servlet-api-2.5</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>jsp-2.1</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>org.mortbay.jetty</groupId>
                                    <artifactId>jsp-api-2.1</artifactId>
                                    <version>${jetty.version}</version>
                                </artifactItem>
                                <artifactItem>
                                    <groupId>ant</groupId>
                                    <artifactId>ant</artifactId>
                                    <version>1.6.5</version>
                                </artifactItem>
                            </artifactItems>
                            <outputDirectory>${project.build.directory}/generated-dirs/lib/web</outputDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy_webapps</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>org.videolan.vlma</groupId>
                                    <artifactId>vlma-webapp</artifactId>
                                    <version>${project.version}</version>
                                    <type>war</type>
                                    <destFileName>vlmaw.war</destFileName>
                                </artifactItem>
                            </artifactItems>
                            <outputDirectory>${project.build.directory}/generated-dirs/webapps</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>org.videolan.vlma</groupId>
            <artifactId>vlma-daemon</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>

</project>
