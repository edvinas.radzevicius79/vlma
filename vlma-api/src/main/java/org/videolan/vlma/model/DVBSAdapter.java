/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;


/**
 * DVB-S card.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 *
 */
public class DVBSAdapter extends DVBAdapter {

    private static final long serialVersionUID = 2L;

    private Satellite satellite;

    /**
     * Constructs a new DVBSAdapter.
     */
    public DVBSAdapter() {
        this.satellite = new Satellite("");
    }

    /**
     * Returns the name describing the adapter type.
     *
     * @return the adapter type name
     */
    public AdapterFamily getFamily() {
        return AdapterFamily.Sat.getInstance(satellite);
    }

    /**
     * Returns the satellite the card uses.
     *
     * @return the satellite the card uses
     */
    public Satellite getSatellite() {
        return satellite;
    }

    /**
     * Sets the satellite the card uses.
     *
     * @param satellite
     *            the satellite the card uses
     */
    public void setSatellite(Satellite satellite) {
        this.satellite = satellite;
    }

    @Override
    public boolean canRead(Media media) {
        if(media == null)
            return false;
        if(media instanceof SatChannel)
            return satellite.getCoverages().contains(((SatChannel) media).getCoverage());
        return false;
    }

}
