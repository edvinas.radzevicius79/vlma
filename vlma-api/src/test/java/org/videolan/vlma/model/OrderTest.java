/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class OrderTest {

    private Order order;
    
    @Test
    public void testEquals() throws Exception {
        DTTChannel media1 = new DTTChannel();
        media1.setName("media1");
        media1.setFrequency(42);
        Program program1 = new Program();
        program1.setMedia(media1);
        program1.setName("pouet");
        program1.setId("pouet");
        media1.addProgram(program1);
        DTTChannel media2 = new DTTChannel();
        media2.setName("media2");
        media2.setFrequency(42);
        Program program2 = new Program();
        program2.setMedia(media2);
        program2.setName("plop");
        program2.setId("plop");
        media2.addProgram(program2);
        ProgramGroup group = new DTTProgramGroup();
        group.add(program1);
        group.add(program2);
        ProgramGroup group2 = new DTTProgramGroup();
        group2.add(program2);
        group2.add(program1);
        Adapter adapter = new DVBTAdapter();
        Server server = new Server();
        server.setName("server");
        server.setAddress("127.0.0.1");
        adapter.setName("adapter");
        adapter.setServer(server);
        Adapter adapter2 = new DVBTAdapter();
        adapter2.setName("adapter");
        adapter2.setServer(server);
        order = new Order(adapter, group);
        Order order2 = new Order(adapter2, group2);
        // Same order because same adapter and adapter.capacity == 1
        assertEquals(order, order2);
        assertEquals(order.getName(), order2.getName());
        assertEquals(order.hashCode(), order2.hashCode());
    }

    @Test
    public void testEquals2() throws Exception {
        FilesChannel m1 = new FilesChannel();
        m1.setName("m1");
        m1.setId(42);
        Program p1 = new Program();
        p1.setMedia(m1);
        p1.setName("pouet");
        p1.setId("pouet");
        m1.addProgram(p1);
        FilesChannel m2 = new FilesChannel();
        m2.setName("m2");
        m2.setId(12);
        Program p2 = new Program();
        p2.setMedia(m2);
        p2.setName("plop");
        p2.setId("plop");
        m2.addProgram(p2);
        ProgramGroup g1 = new FilesProgramGroup();
        g1.add(p1);
        ProgramGroup g2 = new FilesProgramGroup();
        g2.add(p2);
        Adapter a = new FilesAdapter();
        a.setName("adapter");
        Server server = new Server();
        server.setName("server");
        a.setServer(server);
        order = new Order(a, g1);
        Order order2 = new Order(a, g2);
        // Different media -> different order and different order name
        assertFalse(order.equals(order2));
        assertFalse(order.getName().equals(order2.getName()));
    }
}
