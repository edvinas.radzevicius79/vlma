#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import conf, logging, os, streamer, sys, socket

logger = logging.getLogger("watchdog.utils")

platform_is_windows = False
if sys.platform.find("win") >= 0:
  platform_is_windows = True
  try:
    import win32api, win32con, wmi, pythoncom
  except ImportError, e:
    print "You need to install the win32api and the wmi modules for Python in order to run the watchdog"
    sys.exit(1)


def _initWMI():
  # Needed for WMI
  # See http://timgolden.me.uk/python/wmi.html, "Using WMI in a thread or in a Service"
  pythoncom.CoInitialize()

def getCpuLoad():
  """Get the CPU load of the computer"""
  if platform_is_windows:
    _initWMI()
    cpu_load = 0.
    procs = wmi.WMI().query("Select LoadPercentage from Win32_Processor")
    for proc in procs:
      cpu_load += float(proc.LoadPercentage) / 100
    return cpu_load
  else:
    return os.getloadavg()[0]


def _getTrafficX(param):
  try:
    result = os.popen("cat /proc/net/dev")
    result.readline()
    while True:
      line = result.readline()
      if not line:
        return 0.
      elif conf.NETWORK_INTERFACE in line:
        return float(line.replace(":", " ").split()[param])
  except Exception, e:
    logger.warn(e)
    return 0.


def _getTrafficXWin(param):
  try:
    result = os.popen("netstat -e")
    line = result.readline()
    while True:
      line = result.readline()
      if not line:
        return 0.
      else:
        # protect your eyes, the following lines of code are really ugly
        tokens = line.split()
        if len(tokens) == 3:
          try:
            return long(tokens[param])
          except Exception, e:
            logger.warn(e)
            continue
  except Exception, e:
    logger.warn(e)
    return 0.


def getTrafficIn():
  """Get the inbound traffic"""
  if platform_is_windows:
    return _getTrafficXWin(1)
  else:
    return _getTrafficX(1)


def getTrafficOut():
  """Get the outbound traffic"""
  if platform_is_windows:
    return _getTrafficXWin(2)
  else:
    return _getTrafficX(9)


class SystemMetrics:
  """Wrapper around system metrics"""

  def __init__(self):
    self.cpu_load = 0.
    self.traffic_in = 0.
    self.traffic_out = 0.


def get_system_metrics():
  """Get the system metrics"""
  metrics = SystemMetrics()
  metrics.cpu_load = getCpuLoad()
  metrics.traffic_in = getTrafficIn()
  metrics.traffic_out = getTrafficOut()
  return metrics


def signalHandler(signum, frame):
  """Triggered when a signal is sent."""
  logger.warn("Signal %i received", signum)
  for streamer in conf.STREAMERS:
    streamer.stop()
  sys.exit(0)


def check_port(port):
  """Check whether the provided port is available"""
  s = socket.socket()
  s.settimeout(0.5)
  result = False
  try:
    s.connect(("localhost", port))
  except:
    result = True
  finally:
    s.close()
  return result


def which(program):
  """Find the path to a given command"""
  def is_exe(fpath):
    return os.path.exists(fpath) and os.access(fpath, os.X_OK)

  fpath, fname = os.path.split(program)
  if fpath:
    if is_exe(program):
      return program
  else:
    for path in os.environ["PATH"].split(os.pathsep):
      exe_file = os.path.join(path, program)
      if is_exe(exe_file):
        return exe_file

  return None


def is_busy(adapter):
  """Find whether an adapter is busy or not"""
  for s in conf.STREAMERS:
    for order in s.orders.values():
      if isinstance(order, streamer.api.DVBOrder):
        if adapter == order.adapter:
          return True
  return False

