#! /bin/sh

# Add a DVB-S order

curl -d "<dvbs-order><id>orderid</id><ttl>12</ttl><adapter>0</adapter><frequency>0</frequency><fec>9</fec><voltage>13</voltage><srate>0</srate><dvb-bandwidth>8</dvb-bandwidth><programs><program><src>1249</src><dest><ip>0.0.0.0</ip><port>1234</port><streaming><type>broadcast</type><protocol>udp</protocol><mux>raw</mux></streaming></dest><dest><streaming><type>broadcast</type><protocol>udp</protocol><mux>raw</mux></streaming></dest></program><program><src>1250</src><dest><streaming><type>broadcast</type><protocol>udp</protocol><mux>raw</mux></streaming></dest></program></programs></dvbs-order>" --user videolan:admin http://localhost:4213/order/add
